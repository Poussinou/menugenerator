CREATE TABLE category (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name)
);

CREATE INDEX idx_name ON category(name);

CREATE TABLE meal (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name),
  light BOOLEAN NOT NULL,
  meat BOOLEAN NOT NULL,
  month_start TINYINT UNSIGNED NOT NULL,
  month_end TINYINT UNSIGNED NOT NULL,
  source varchar(500) NOT NULL,
  category_id BIGINT UNSIGNED NOT NULL REFERENCES category(id)
);

CREATE INDEX idx_name ON meal(name);
