import { writable, get } from 'svelte/store';
import { toast } from '@zerodevx/svelte-toast';
import { fetch as tauri_fetch } from '@tauri-apps/plugin-http';
import { getTauriVersion } from '@tauri-apps/api/app';

export const backend = writable(null);
const tauri = writable(null);

export const months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];

export async function set_running_with_tauri() {
	try {
		const version = await getTauriVersion();
		console.log('With tauri', version);
		tauri.set(true);
	} catch {
		console.log('Without tauri');
		tauri.set(false);
	}
}

export async function reset_backend() {
	backend.set(null);
	localStorage.removeItem('backend_address');
	await call_backend('DELETE', '/backend', null, 'Failed to reset backend');
	init_backend();
}

export async function own_fetch(...args) {
	while (get(tauri) == null) {
		await new Promise((r) => setTimeout(r, 100));
	}
	args[0] = get_server() + args[0];
	if (get(tauri)) {
		return tauri_fetch(...args);
	} else {
		return fetch(...args);
	}
}

export async function init_backend() {
	await set_running_with_tauri();

	// Already init
	let local_host = window.location.host.includes('localhost');
	if (get(backend) !== null || !local_host) {
		return;
	}

	// Check if already set
	const response_get = await own_fetch('/backend');
	if (response_get.ok) {
		backend.set(await response_get.text());
		return;
	}

	// Ask for the backend
	let backend_address = localStorage.getItem('backend_address');
	if (backend_address === null) {
		backend_address = prompt('Please enter the backend server address');
		if (backend_address === null) {
			return;
		}
	}

	let encoded = encodeURIComponent(backend_address);
	await call_backend('POST', `/backend/${encoded}`, null, 'Failed to set the backend');

	localStorage.setItem('backend_address', backend_address);
	backend.set(backend_address);
}

export function get_server() {
	let current = window.location.origin;
	if (current.includes('localhost')) return 'http://localhost:8000';
	else return current;
}

export async function call_backend(method, path, body, error_message, no_answer = false) {
	if (body !== null) {
		body = JSON.stringify(body);
	}
	const response = await own_fetch(path, {
		method: method,
		body: body
	});
	if (!response.ok) {
		let text = await response.text();
		toast.push(`${error_message}: ${text}`);
		return null;
	}
	if (no_answer) {
		return true;
	}
	return await response.json();
}

export async function get_categories() {
	let categories = await call_backend('GET', '/category', null, 'Failed to fetch categories');
	return categories.map((x) => {
		return { value: x.id, name: x.name };
	});
}

export async function get_meals(month = null) {
	if (month === null) {
		return await call_backend('GET', '/meal', null, 'Failed to fetch meals');
	} else {
		return await call_backend('GET', `/meal/month/${month}`, null, 'Failed to fetch meals');
	}
}

export function convert_categories_to_select(categories) {
	return categories.map((x) => {
		x.quantity = meals.reduce((acc, cur) => (acc += cur.category_id == x.value), 0);
		return x;
	});
}
