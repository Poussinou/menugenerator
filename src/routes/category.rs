use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::category::Category;
use crate::config::Config;
use crate::database_trait::DatabaseTrait;
use crate::menu_error::MenuError;

#[cfg(feature = "diesel")]
use crate::database::Database;

#[cfg(feature = "tauri")]
use crate::local_database::LocalDatabase;
#[cfg(feature = "tauri")]
use reqwest::{self, StatusCode};

#[cfg(feature = "diesel")]
const DATABASE_ERROR: &str = "Database not set";

pub fn get_routes() -> Vec<Route> {
    routes![get_categories, add_category, delete_category]
}

#[get("/")]
pub async fn get_categories(
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Category>>, MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        // Just fetch the data from the db
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        return Ok(Json(db.get_categories()));
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            Ok(Json(db.get_categories()))
        }
        // Remote server
        else {
            let url = url.unwrap();
            let resp = reqwest::get(format!("{url}/category")).await?;

            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }

            // Save the data
            let data: Vec<Category> = resp.json().await?;
            db.save_categories(&data)?;

            Ok(Json(data))
        }
    }
}

#[put("/", data = "<category>")]
pub async fn add_category(
    category: Json<Category>,
    config: &State<Mutex<Config>>,
) -> Result<Json<u64>, MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        let category = category.into_inner();
        let id = db.add_category(&category)?;
        return Ok(Json(id));
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            let id = db.add_category(&category)?;
            Ok(Json(id))
        }
        // Remote server
        else {
            let url = url.unwrap();
            let client = reqwest::Client::new();
            let resp = client
                .post(format!("{url}/category"))
                .json(&category.into_inner())
                .send()
                .await?;
            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }

            let output: u64 = resp.json().await?;
            Ok(Json(output))
        }
    }
}

#[delete("/<id>")]
pub async fn delete_category(id: u64, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        return db.delete_category(id);
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            db.delete_category(id)
        }
        // Remote server
        else {
            let url = url.unwrap();
            let client = reqwest::Client::new();
            let resp = client
                .delete(format!("{url}/category"))
                .json(&id)
                .send()
                .await?;
            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }
            Ok(())
        }
    }
}
