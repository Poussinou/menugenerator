use rocket::tokio::sync::Mutex;
use rocket::{delete, get, http::Status, post, routes, Route, State};

use crate::config::Config;
use crate::menu_error::MenuError;

pub fn get_routes() -> Vec<Route> {
    routes![set_backend, get_backend, delete_backend]
}

#[post("/backend/<url>")]
async fn set_backend(url: &str, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let mut config = config.inner().lock().await;
    config.set_backend_url(url)
}

#[delete("/backend")]
async fn delete_backend(config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let mut config = config.inner().lock().await;
    config.delete_backend_url()
}

#[get("/backend")]
async fn get_backend(config: &State<Mutex<Config>>) -> (Status, String) {
    let config = config.inner().lock().await;
    let url = config.backend_url();
    if url.is_some() {
        (Status::Ok, url.as_ref().unwrap().to_string())
    } else {
        (Status::NotFound, "Backend not set".to_string())
    }
}
