use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, put, routes, Route, State};

use crate::config::Config;
use crate::database_trait::DatabaseTrait;
use crate::meal::Meal;
use crate::menu_error::MenuError;

#[cfg(feature = "diesel")]
use crate::database::Database;

#[cfg(feature = "tauri")]
use crate::local_database::LocalDatabase;
#[cfg(feature = "tauri")]
use reqwest::StatusCode;

#[cfg(feature = "diesel")]
const DATABASE_ERROR: &str = "Database not set";

pub fn get_routes() -> Vec<Route> {
    routes![get_meals, get_meals_in_month, add_meal, delete_meal]
}

#[get("/")]
pub async fn get_meals(config: &State<Mutex<Config>>) -> Result<Json<Vec<Meal>>, MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        Ok(Json(db.get_meals()))
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            Ok(Json(db.get_meals()))
        }
        // Remote server
        else {
            let url = url.unwrap();
            let resp = reqwest::get(format!("{url}/meal")).await?;

            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }

            // Save the data
            let data: Vec<Meal> = resp.json().await?;
            db.save_meals(&data)?;

            Ok(Json(data))
        }
    }
}

#[get("/month/<month>")]
pub async fn get_meals_in_month(
    month: u8,
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<Meal>>, MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        return Ok(Json(db.get_meals_in_month(month)));
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            Ok(Json(db.get_meals_in_month(month)))
        }
        // Remote server
        else {
            let url = url.unwrap();
            let resp = reqwest::get(format!("{url}/meal/month/{month}")).await?;

            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }

            let data: Vec<Meal> = resp.json().await?;
            Ok(Json(data))
        }
    }
}

#[put("/", data = "<meal>")]
pub async fn add_meal(
    meal: Json<Meal>,
    config: &State<Mutex<Config>>,
) -> Result<Json<u64>, MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        let meal = meal.into_inner();
        let id = db.add_meal(&meal)?;
        return Ok(Json(id));
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            Ok(Json(db.add_meal(&meal)?))
        }
        // Remote server
        else {
            let url = url.unwrap();
            let client = reqwest::Client::new();
            let resp = client
                .put(format!("{url}/meal"))
                .json(&meal.into_inner())
                .send()
                .await?;

            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }
            let data = resp.json().await?;
            Ok(Json(data))
        }
    }
}

#[delete("/<id>")]
pub async fn delete_meal(id: u64, config: &State<Mutex<Config>>) -> Result<(), MenuError> {
    let config = config.inner().lock().await;
    #[cfg(feature = "diesel")]
    {
        let url = config
            .database_url()
            .clone()
            .ok_or(MenuError::Menu(DATABASE_ERROR.to_string()))?;
        let mut db = Database::new(&url);
        return db.delete_meal(id);
    }
    #[cfg(feature = "tauri")]
    {
        // Get some parameters
        let url = config.backend_url().clone();
        let mut db = LocalDatabase::new(config.config_dir());

        // Local data
        if url.is_none() {
            db.delete_meal(id)
        }
        // Remote server
        else {
            let url = url.unwrap();
            let client = reqwest::Client::new();
            let resp = client.delete(format!("{url}/meal/{id}")).send().await?;

            if resp.status() != StatusCode::OK {
                let text = resp.text().await?;
                return Err(MenuError::Menu(text));
            }
            Ok(())
        }
    }
}
