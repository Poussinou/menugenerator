use crate::{category::Category, meal::Meal, menu_error::MenuError};

pub trait DatabaseTrait {
    fn get_meals(&mut self) -> Vec<Meal>;
    fn get_meals_in_month(&mut self, current: u8) -> Vec<Meal>;
    fn add_meal(&mut self, meal: &Meal) -> Result<u64, MenuError>;
    fn get_meal_by_name(&mut self, name: &String) -> Result<Option<Meal>, MenuError>;
    fn delete_meal(&mut self, id: u64) -> Result<(), MenuError>;
    fn get_categories(&mut self) -> Vec<Category>;
    fn add_category(&mut self, category: &Category) -> Result<u64, MenuError>;
    fn delete_category(&mut self, id: u64) -> Result<(), MenuError>;
}
