#[cfg(feature = "diesel")]
use diesel::result::Error as DieselError;
use rocket::http::Status;
use rocket::response::Responder;
use rocket::tokio::task::JoinError;
use rocket::{response, Request};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum MenuError {
    #[error("MenuGenerator error: {0}")]
    Menu(String),

    #[error("MenuGenerator error: {0}")]
    MenuStr(&'static str),

    #[error("Join error: {0}")]
    Join(#[from] JoinError),

    #[error("Failed to process IO: {0}")]
    IOError(#[from] std::io::Error),

    #[error("Failed to convert json: {0}")]
    SerdeError(#[from] serde_json::Error),

    #[cfg(feature = "tauri")]
    #[error("Request failed: {0}")]
    Reqwest(#[from] reqwest::Error),

    #[cfg(feature = "diesel")]
    #[error("SQL error: {0}")]
    Diesel(#[from] DieselError),
}

impl<'r> Responder<'r, 'static> for MenuError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let string = format!("{}", self);
        let status = match self {
            _ => Status::InternalServerError,
        };
        response::Response::build_from(string.respond_to(req)?)
            .status(status)
            .ok()
    }
}
