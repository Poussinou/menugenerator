#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::prelude::Insertable;
#[cfg(feature = "diesel")]
use diesel::{AsChangeset, Selectable};
use serde::{Deserialize, Serialize};

#[cfg_attr(
    feature = "diesel",
    derive(Selectable, Queryable, AsChangeset, Insertable)
)]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::meal))]
#[derive(Deserialize, Serialize, Clone)]
pub struct Meal {
    #[cfg_attr(feature="diesel", diesel(deserialize_as = u64))]
    pub id: Option<u64>,
    pub name: String,
    pub light: bool,
    pub meat: bool,
    pub month_start: u8,
    pub month_end: u8,
    pub source: String,
    pub category_id: u64,
}
