use serde::{Deserialize, Serialize};
use std::str;
use std::{fs::File, path::PathBuf};

use crate::menu_error::MenuError;

const CONFIG_FILE: &str = "config.json";

#[derive(Serialize, Deserialize)]
pub struct Config {
    database_url: Option<String>,
    backend_url: Option<String>,
    expose_rocket: bool,
    frontend_path: String,
    config_dir: String,
}

impl Config {
    pub fn new(config_dir: &PathBuf) -> Self {
        let default = Self {
            frontend_path: "frontend/build".to_string(),
            expose_rocket: false,
            backend_url: None,
            database_url: None,
            config_dir: config_dir.to_str().unwrap().to_string(),
        };

        let config_file = config_dir.join(CONFIG_FILE);
        let file = match File::open(config_file) {
            Ok(f) => f,
            Err(_) => {
                default.save().unwrap();
                return default;
            }
        };

        let config = match serde_json::from_reader(file) {
            Err(_) => default,
            Ok(c) => {
                println!("Successfully read configuration file");
                c
            }
        };
        config.save().unwrap();
        config
    }

    pub fn save(&self) -> Result<(), MenuError> {
        let filename = PathBuf::from(self.config_dir.clone()).join(CONFIG_FILE);
        let file = File::create(filename)?;
        serde_json::to_writer(file, self)?;
        Ok(())
    }

    pub fn set_backend_url(&mut self, url: &str) -> Result<(), MenuError> {
        self.backend_url = Some(String::from(url));
        self.save()
    }

    pub fn backend_url(&self) -> Option<String> {
        self.backend_url.clone()
    }

    pub fn delete_backend_url(&mut self) -> Result<(), MenuError> {
        self.backend_url = None;
        self.save()
    }

    pub fn expose_rocket(&self) -> bool {
        self.expose_rocket
    }

    pub fn database_url(&self) -> &Option<String> {
        &self.database_url
    }

    pub fn frontend_path(&self) -> &String {
        &self.frontend_path
    }

    pub fn config_dir(&self) -> &String {
        &self.config_dir
    }
}
