use diesel::mysql::MysqlConnection;
use diesel::BoolExpressionMethods;
use diesel::Connection;
use diesel::{ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl, SelectableHelper};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

use crate::category::Category;
use crate::database_trait::DatabaseTrait;
use crate::meal::Meal;
use crate::menu_error::MenuError;
use crate::schema::{category, meal};

pub struct Database {
    sql: MysqlConnection,
}

impl Database {
    pub fn new(url: &String) -> Self {
        Self {
            sql: MysqlConnection::establish(url).unwrap(),
        }
    }

    pub fn migrate(&mut self) {
        // Migrate DB
        const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations/");
        self.sql
            .run_pending_migrations(MIGRATIONS)
            .expect("Failed to run migration");
    }

    pub fn get_category_by_name(&mut self, name: &String) -> Result<Option<Category>, MenuError> {
        let category = category::table
            .select(Category::as_select())
            .filter(category::name.eq(name))
            .first(&mut self.sql)
            .optional()?;
        Ok(category)
    }
}

impl DatabaseTrait for Database {
    // Meal
    fn get_meals(&mut self) -> Vec<Meal> {
        meal::table
            .select(Meal::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch meals")
    }

    fn get_meals_in_month(&mut self, current: u8) -> Vec<Meal> {
        // start <= current <= end
        let middle = meal::month_start
            .le(current)
            .and(meal::month_end.ge(current));
        //  current <= end <= start
        let begin = meal::month_end
            .ge(current)
            .and(meal::month_start.ge(meal::month_end));
        //  current >= start >= end
        let end = meal::month_start
            .le(current)
            .and(meal::month_end.le(meal::month_start));
        meal::table
            .select(Meal::as_select())
            .filter(middle)
            .or_filter(begin)
            .or_filter(end)
            .load(&mut self.sql)
            .expect("Failed to fetch meals")
    }
    fn add_meal(&mut self, meal: &Meal) -> Result<u64, MenuError> {
        diesel::insert_into(meal::table)
            .values(meal)
            .execute(&mut self.sql)?;
        let category = self.get_meal_by_name(&meal.name)?;
        Ok(category.unwrap().id.unwrap())
    }

    fn get_meal_by_name(&mut self, name: &String) -> Result<Option<Meal>, MenuError> {
        let meal = meal::table
            .select(Meal::as_select())
            .filter(meal::name.eq(name))
            .first(&mut self.sql)
            .optional()?;
        Ok(meal)
    }

    fn delete_meal(&mut self, id: u64) -> Result<(), MenuError> {
        diesel::delete(meal::table.filter(meal::id.eq(id))).execute(&mut self.sql)?;
        Ok(())
    }

    // Category
    fn get_categories(&mut self) -> Vec<Category> {
        category::table
            .select(Category::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch categories")
    }

    fn add_category(&mut self, category: &Category) -> Result<u64, MenuError> {
        diesel::insert_into(category::table)
            .values(category)
            .execute(&mut self.sql)?;
        let category = self.get_category_by_name(&category.name)?;
        Ok(category.unwrap().id.unwrap())
    }

    fn delete_category(&mut self, id: u64) -> Result<(), MenuError> {
        diesel::delete(category::table.filter(category::id.eq(id))).execute(&mut self.sql)?;
        Ok(())
    }
}
