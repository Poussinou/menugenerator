use std::fs::File;
use std::path::PathBuf;

use crate::{category::Category, database_trait::DatabaseTrait, meal::Meal, menu_error::MenuError};

pub struct LocalDatabase {
    local_path: PathBuf,
}

impl LocalDatabase {
    pub fn new(local_path: &String) -> Self {
        Self {
            local_path: PathBuf::from(local_path),
        }
    }

    pub fn get_category_filename(&self) -> PathBuf {
        const CATEGORY_FILENAME: &str = "category.json";
        self.local_path.join(CATEGORY_FILENAME)
    }

    pub fn get_meal_filename(&self) -> PathBuf {
        const MEAL_FILENAME: &str = "meal.json";
        self.local_path.join(MEAL_FILENAME)
    }

    pub fn save_categories(&self, categories: &Vec<Category>) -> Result<(), MenuError> {
        let filename = self.get_category_filename();
        let file = File::create(filename)?;
        serde_json::to_writer(file, categories)?;
        Ok(())
    }

    pub fn save_meals(&self, meals: &Vec<Meal>) -> Result<(), MenuError> {
        let filename = self.get_meal_filename();
        let file = File::create(filename)?;
        serde_json::to_writer(file, meals)?;
        Ok(())
    }
}

impl DatabaseTrait for LocalDatabase {
    fn get_meals(&mut self) -> Vec<Meal> {
        let filename = self.get_meal_filename();
        let file = File::open(filename).unwrap();
        serde_json::from_reader(file).unwrap()
    }

    fn get_meals_in_month(&mut self, current: u8) -> Vec<Meal> {
        let meals = self.get_meals();
        meals
            .iter()
            .map(|v| v.clone())
            .filter(|x| {
                (x.month_start < current && current < x.month_end)
                    || (current <= x.month_end && x.month_end <= x.month_start)
                    || (current >= x.month_start && x.month_start >= x.month_end)
            })
            .collect()
    }

    fn add_meal(&mut self, meal: &Meal) -> Result<u64, MenuError> {
        let mut meal = meal.clone();
        let mut meals = self.get_meals();
        let max_id = meals.iter().max_by_key(|x| x.id).unwrap().id;
        meal.id = Some(max_id.unwrap() + 1);
        meals.push(meal.clone());
        self.save_meals(&meals)?;
        Ok(meal.id.unwrap())
    }

    fn get_meal_by_name(&mut self, name: &String) -> Result<Option<Meal>, MenuError> {
        let meals = self.get_meals();
        let meal = meals.iter().find(|&x| x.name == *name);
        Ok(meal.cloned())
    }

    fn delete_meal(&mut self, id: u64) -> Result<(), MenuError> {
        let mut meals = self.get_meals();
        let pos = meals
            .iter()
            .position(|x| x.id.unwrap() == id)
            .ok_or(MenuError::Menu("Cannot find required id".to_string()))?;

        meals.remove(pos);
        self.save_meals(&meals)?;
        Ok(())
    }

    fn get_categories(&mut self) -> Vec<Category> {
        let filename = self.get_category_filename();
        let file = File::open(filename).unwrap();
        serde_json::from_reader(file).unwrap()
    }

    fn add_category(&mut self, category: &Category) -> Result<u64, MenuError> {
        let mut category = category.clone();
        let mut cat = self.get_categories();
        let max_id = cat.iter().max_by_key(|x| x.id).unwrap().id;
        category.id = Some(max_id.unwrap() + 1);
        cat.push(category.clone());
        self.save_categories(&cat)?;
        Ok(category.id.unwrap())
    }

    fn delete_category(&mut self, id: u64) -> Result<(), MenuError> {
        let mut cat = self.get_categories();
        let pos = cat
            .iter()
            .position(|x| x.id.unwrap() == id)
            .ok_or(MenuError::Menu("Cannot find required id".to_string()))?;

        cat.remove(pos);
        self.save_categories(&cat)?;
        Ok(())
    }
}
