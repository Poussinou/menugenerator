use rocket::tokio::sync::Mutex;
use rocket::{http::Method, Build, Rocket};
use rocket_cors::{AllowedOrigins, CorsOptions};
use std::net::Ipv4Addr;
use std::path::PathBuf;

#[cfg(feature = "diesel")]
use futures::executor::block_on;

use crate::config::Config;
use crate::routes::{category, general, meal};

pub fn get_rocket(config_dir: PathBuf) -> Rocket<Build> {
    // Config
    let config = Config::new(&config_dir);
    let ip = if config.expose_rocket() {
        Ipv4Addr::new(0, 0, 0, 0)
    } else {
        Ipv4Addr::new(127, 0, 0, 1)
    };
    let config: Mutex<Config> = Mutex::new(config);

    let rocket_config = rocket::Config::figment()
        .merge(("address", ip))
        .merge(("log_level", "normal"));

    // Migrate DB
    #[cfg(feature = "diesel")]
    {
        let config = block_on(config.lock());
        if config.backend_url().is_none() {
            use crate::database::Database;
            let url = config.database_url().clone().expect("Database not set");
            Database::new(&url).migrate();
        }
    }

    // Cors
    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![
                Method::Get,
                Method::Post,
                Method::Patch,
                Method::Put,
                Method::Delete,
            ]
            .into_iter()
            .map(From::from)
            .collect(),
        )
        .allow_credentials(true);

    rocket::custom(rocket_config)
        .manage(config)
        .mount("/category", category::get_routes())
        .mount("/meal", meal::get_routes())
        .mount("/", general::get_routes())
        .attach(cors.to_cors().unwrap())
}
