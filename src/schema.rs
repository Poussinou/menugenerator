// @generated automatically by Diesel CLI.

diesel::table! {
    category (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
    }
}

diesel::table! {
    meal (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
        light -> Bool,
        meat -> Bool,
        month_start -> Unsigned<Tinyint>,
        month_end -> Unsigned<Tinyint>,
        #[max_length = 500]
        source -> Varchar,
        category_id -> Unsigned<Bigint>,
    }
}

diesel::allow_tables_to_appear_in_same_query!(category, meal,);
