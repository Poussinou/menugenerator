# MenuGenerator
[![discord]([https://img.shields.io/badge/Discord-7289DA?style=for-the-badge&logo=discord&logoColor=white)](https://discord.gg/ZFstbnfppZ)


This repository contains a python web service to manage a list of meal according to the season.


## Screenshots

![meals](fastlane/metadata/android/en-US/images/phoneScreenshots/meals.png){width=25% height=25%}
![categories](fastlane/metadata/android/en-US/images/phoneScreenshots/categories.png){width=25% height=25%}
